module.exports = function (context) {

    if (!context) throw new Error("can not start the application without context");
    if (!context.config) throw new Error("can not start the application without configuration");


    console.log(context.cluster.worker.process.pid);

    // express net.Server
    var app = require('express')();

    // CORS filter
    app.use(require('cors')({
        allowedHeaders: ['Accept', 'x-kokoriko', 'Content-Type']
    }));

    // nsa security filter
    app.use(require('./lib/globals/nsa'));

    // content negotiation filter
    app.use(require('./lib/globals/content-negotiation'));

    // hal navigation router endpoint
    app.use('/', require('./lib/router/hal')() );

    // API
    // hal service router endpoint, geo search
    app.use(context.config.get("server.contextPath"), require('./lib/router/api/geo')(context));

    // hal service router endpoint, geo criteria day search
    app.use(context.config.get("server.contextPath"), require('./lib/router/api/geoCriteriaDay')(context));

    // error handler filter
    //app.use(require('./lib/globals/error'));

// START THE SERVER
// =============================================================================
    app.listen(context.config.get("server.port"), context.config.get("server.address"), function () {
        console.log('mfoodpoint-search-api listening on port: ' + context.config.get("server.port"));

    });
};
