var gulp   = require('gulp');
var clean  = require('gulp-clean');
var merge  = require('merge-stream');
var git    = require('gulp-git');
var rename = require('gulp-rename');
var mocha  = require('gulp-mocha');

gulp.task('clean', function () {
    return gulp.src('build', {read: false})
        .pipe(clean({force:true}));
});

gulp.task('app', ['clean'], function(){
    git.revParse({args: '--abbrev-ref HEAD', cwd: './'},function(err,branch){

        console.log("branch name: " + branch);

        return gulp.src(['mfoodpoint-search-api.js'])
            .pipe(rename('mfoodpoint-search-api-' + branch + '.js'))
            .pipe(gulp.dest('build/libs/'));
    });
});

gulp.task('libraries', ['clean'],function () {
    var lib = gulp.src('lib/**')
        .pipe(gulp.dest('build/libs/lib'));

    // '!app/{_tmp,_tmp/**}'
    var node_modules = gulp
        .src(['node_modules/**',
            '!node_modules/{gulp*,gulp*/**}', // exclude gulp packaging
            '!node_modules/{sinon*,sinon*/**}', // exclude sinon testing
            '!node_modules/{mocha*,mocha*/**}', // exclude mocha testing
            '!node_modules/{chai*,chai*/**}', // exclude chai testing
            '!node_modules/{rewire*,rewire*/**}']) // exclude rewire testing
        .pipe(gulp.dest('build/libs/node_modules'));

    var conf = gulp.src(['config/**'])
        .pipe(gulp.dest('build/libs/config'));

    return merge(lib, node_modules, conf);
});

//gulp.task('build', ['clean','app','libraries', 'test']);
// i deploy new version for frontend before the tests are green cause HULK
gulp.task('build', ['clean','app','libraries']);

gulp.task('test', function () {
    return gulp.src('test/**/*.js', {read: false})
        // gulp-mocha needs filepaths so you can't have any plugins before it
        .pipe( mocha({reporter: 'nyan'}) )
});

