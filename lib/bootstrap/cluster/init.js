/**
 * Created by virus on 11/24/15.
 */
module.exports = function (context, next) {

    cluster(context, next)
};

function cluster(context, cb){
    var cluster = require('cluster');
    var numCPUs = require('os').cpus().length;

    context.cluster = context.cluster || {};

    if (cluster.isMaster) {
        for (var i = 0; i < numCPUs; i++) {
            cluster.fork();
        }
    } else {
        context.cluster.worker = cluster.worker;
        cb(context);
    }

}