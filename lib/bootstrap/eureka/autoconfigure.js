var os      = require("os");
var ip      = require("ip");
var urljoin = require('url-join');

var validator = require("../../util/validator")();

var tasks = [

    /**
     * instance.hostName
     * @param eurekaConfigObject
     * @param conf
     * @returns {*}
     */
    function hostname(eurekaConfigObject, conf) {

        eurekaConfigObject.instance.hostName = eurekaConfigObject.instance.hostName ||
            conf.get("application.name") + '.' + os.hostname();

        
    },

    /**
     * instance.app
     * @param eurekaConfigObject
     * @param conf
     */
    function app(eurekaConfigObject, conf) {

        eurekaConfigObject.instance.app = eurekaConfigObject.instance.app ||
            conf.get("application.name");

        

    },

    /**
     * instance.ipAddr
     * instance.vipAddress
     * instance.secureVipAddress
     * @param eurekaConfigObject
     * @param conf
     * @returns {*}
     */
    function address(eurekaConfigObject, conf) {
        eurekaConfigObject.instance.ipAddr = eurekaConfigObject.instance.ipAddr ||
            ip.address("private");

        eurekaConfigObject.instance.vipAddress = eurekaConfigObject.instance.vipAddress ||
            ip.address("private");

        eurekaConfigObject.instance.secureVipAddress = eurekaConfigObject.instance.secureVipAddress ||
            ip.address("private");

    },

    /**
     * instance.port
     * instance.securePort
     * @param eurekaConfigObject
     * @param conf
     * @returns {*}
     */
    function port(eurekaConfigObject, conf) {
        eurekaConfigObject.instance.port = eurekaConfigObject.instance.port ||
            conf.get("server.port");

        eurekaConfigObject.instance.securePort = eurekaConfigObject.instance.securePort ||
            443;

        
    },

    /**
     * instance.homePageUrl
     * instance.statusPageUrl
     * instance.healthCheckUrl
     * @param eurekaConfigObject
     * @param conf
     * @returns {*}
     */
    function url(eurekaConfigObject, conf) {

        eurekaConfigObject.instance.homePageUrl = eurekaConfigObject.instance.homePageUrl ||
            urljoin(
                conf.get("server.protocol"),
                eurekaConfigObject.instance.hostName +':'+ eurekaConfigObject.instance.port,
                conf.get("server.contextPath")
            );

        eurekaConfigObject.instance.statusPageUrl = eurekaConfigObject.instance.statusPageUrl ||
            urljoin(
                eurekaConfigObject.instance.homePageUrl,
                '/info'
            );

        eurekaConfigObject.instance.healthCheckUrl = eurekaConfigObject.instance.healthCheckUrl ||
            urljoin(
                eurekaConfigObject.instance.homePageUrl,
                '/status'
            );

        
    },

    function dataCenter(eurekaConfigObject, conf){
        eurekaConfigObject.instance.dataCenterInfo = eurekaConfigObject.instance.dataCenterInfo || {};

        eurekaConfigObject.instance.dataCenterInfo.name = eurekaConfigObject.instance.dataCenterInfo.name ||
                "MyOwn"
    },

    /**
     * eureka.host
     * eureka.port
     * eureka.servicePath
     * @param eurekaConfigObject
     * @param conf
     * @returns {*}
     */
    function eureka(eurekaConfigObject, conf) {

        eurekaConfigObject.eureka.host        = eurekaConfigObject.eureka.host || 'speiseplan.menu';
        eurekaConfigObject.eureka.port        = eurekaConfigObject.eureka.port || '10010';
        eurekaConfigObject.eureka.servicePath = eurekaConfigObject.eureka.servicePath || '/eureka/apps/';
    }
];

module.exports = function (context) {

    validator.notNull(context,"can not autocomplete empty context");
    validator.notNull(context.config,"can not autocomplete empty context configuration");
    validator.notNull(context.config.get("instance"),"can not autocomplete empty instance");
    validator.notNull(context.config.get("eureka"),"can not autocomplete empty eureka");

    var populate = {
        instance: context.config.get("instance"),
        eureka: context.config.get("eureka")
    };    

    var i = 0;
    while (i < tasks.length) {
        tasks[i](populate, context.config);
        i++
    }

    return populate
};