var Eureka        = require('eureka-js-client').Eureka;
var autoconfigure = require('./autoconfigure');


module.exports = function (context, next) {

    if( context.config.get("application.features").split(",").indexOf("eureka") < 0 ) return next(context);

    var client = new Eureka( autoconfigure(context) );

    // unregister the client
    process.on( "SIGINT", function() {
        console.log('CLOSING [SIGINT]');
        console.log("unregister eureka");

        client.stop();
    } );
    process.on('exit', function(code) {
        console.log("CLOSING EXIT");
        console.log("unregister eureka");

        client.stop();
    });

    // register the client
    // make it available in configuration-applicationContext object :)
    client.start( function() {
        context.eureka = context.eureka || {};
        context.eureka.discovery = client;

        return next(context);
    });
};