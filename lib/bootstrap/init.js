// util
var fifo     = require('./util/fifo')();

// init middleware
// eureka discovery service registry
var eureka   = require('./eureka/init');
// services dependency injection
var services = require('./services/init');
//
var cluster = require('./cluster/init');

module.exports = function (context, callback) {

    fifo.use(cluster);
    fifo.use(eureka);
    fifo.use(services);

    fifo.run( context, callback );
};