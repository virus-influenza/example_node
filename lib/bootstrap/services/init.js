
module.exports = function (context, next) {
    if (!context || !context.config) throw new Error("can not initialize services with empty configuration");

    if( context.config.get("application.features").split(",").indexOf("services") < 0 ) return next(context);

    context.services = context.services || {};

    context.services.search = require('../../service/search')(context);

    next(context)

};