module.exports = fifo;

function fifo(){

    function impl(){}

    impl.stack = [];

    impl.handle = function(state, callback){
        var index = 0;

        function next(){
            var task = impl.stack[index++];

            if(!task){
                return callback(state)
            }

            task(state, next);
        }

        next();
    };

    impl.use = function(callback){
        this.stack.push(callback);
    };

    impl.run = function(state, callback){
        impl.handle(state,callback);
    };

    return impl;
}