const logger = require('../util/simpleLogger');

var external = require('./provider/external');
var features = require('./provider/features');

var fifo     = require('./util/fifo')();

/*
    npm config filename conventions

 default.EXT
 default-{instance}.EXT
 {deployment}.EXT
 {deployment}-{instance}.EXT
 {short_hostname}.EXT
 {short_hostname}-{instance}.EXT
 {short_hostname}-{deployment}.EXT
 {short_hostname}-{deployment}-{instance}.EXT
 {full_hostname}.EXT
 {full_hostname}-{instance}.EXT
 {full_hostname}-{deployment}.EXT
 {full_hostname}-{deployment}-{instance}.EXT
 local.EXT
 local-{instance}.EXT
 local-{deployment}.EXT
 local-{deployment}-{instance}.EXT
 */
module.exports = function init(initialContext, callback) {

    var context    = initialContext || {};
    context.config = context.config || {};

    console.log( logger.title("::init configuration::"));
    context.config = require("config");

    console.log(logger.title("::initial configuration from files::"));
    console.log(logger.debug("%s"), JSON.stringify(context.config, null, 5));

    fifo.use(features);
    fifo.use(external);

    fifo.run( context, callback );
};
