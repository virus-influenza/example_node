/**
 * @author virus.influenza
 */
var urljoin   = require('url-join');
var request   = require('request');
const logger = require('../../util/simpleLogger');

/**
 * merges current configuration with external provider
 */
module.exports = function(context, next){

    console.log(logger.title("::config chain, external middleware::"));

    if ( !context || ! context.config) throw new Error("can not merge external configuration on empty object");
    console.log(logger.title("::features available to be loaded::"));
    console.log(logger.debug("%s"), JSON.stringify(context.config.get("cloud"), null, 5));

    // defaults
    // values from default.yaml ( bootstrap.yml)
    var init = {};
    init.uri      = context.config.get("cloud.config.uri")      || 'http://localhost:8080';
    init.label    = context.config.get("cloud.config.label")    || 'master';
    init.profiles = context.config.get("cloud.config.profiles") || 'prod';
    init.name     = context.config.get("application.name")      || 'node_application';

    console.log('configuration for external provider: ' + JSON.stringify(init));

    var target = urljoin(
        init.uri,
        init.name,
        init.profiles,
        init.label
    );

    console.log('configuration for external provider, config service uri: ' + target);

    request
        .get(target, function(error, response, body){
            console.log('response from config server: ' + body);

            if ( error){
                console.log('error connecting to config server using file config');

                return next( context );
            }

            var fromServer = JSON.parse(body);
            if ( ! fromServer || ! fromServer.propertySources){
                console.log('error with config file returned by config server using file config');

                return next( context );
            }

            for ( var remote in fromServer.propertySources ){
                console.log('fromServer.propertySources[remote]: '+ JSON.stringify( fromServer.propertySources[remote]) );
                context.config = context.config.util.extendDeep(config, fromServer.propertySources[remote].source );
            }

            console.log(logger.title("::configuration merged with external source::"));
            console.log(logger.debug("%s"), JSON.stringify(context.config.get("cloud"), null, 5));

            return next(context);
        })
};