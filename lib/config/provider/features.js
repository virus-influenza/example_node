const logger = require('../../util/simpleLogger');

var Path = require('path');
/**
 * merges the features configuration into  the configuration file
 * @param context initial config file
 * @param next
 * @returns {*} config object with features merged
 */
module.exports = function(context, next){

    console.log(logger.title("::config chain, features middleware::"));

    if ( !context || !context.config) throw new Error("can not apply features to an empty configuration");

    var features = context.config.get("application.features") || context.config.util.getEnv("NODE_APP_FEATURES");
    console.log(logger.title("::features available to be loaded::"));
    console.log(logger.debug("%s"), JSON.stringify(features, null, 5));

    if ( !features ) return next(context);

    var aFeatures = features.split(",");
    if ( aFeatures.length <= 0 ) return next(context);

    var fullFileName;
    var configObj;
    var extension      = ".yaml"; // PLEASE!
    var baseConfigPath = Path.join( context.config.util.getEnv('NODE_CONFIG_DIR'), "features") ;
    for ( var p in aFeatures ) {

        fullFileName = Path.join (baseConfigPath , aFeatures[p] ) + extension;

        configObj = context.config.util.parseFile(fullFileName);

        if (configObj) {
            context.config = context.config.util.extendDeep(context.config, configObj);
        }
    }

    console.log(logger.title("::configuration after features applied::"));
    console.log(logger.debug("%s"), JSON.stringify(context.config, null, 5));

    return next(context);
};