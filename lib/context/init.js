const configInit = require('../config/init');
const bootInit   = require('../bootstrap/init');
const events     = require('events');

module.exports = function init(initial, cb){

    var context  = initial || {};

    init.dealer   = new events.EventEmitter();
    init.callback = cb || function(context){ init.dealer.emit("CONTEXT_FINISHED", context, "context init finished") };

    // callback
    configInit(context, function(context) {
        bootInit(context, init.callback)
    });

    return init;
};

