var validator  = require('../util/validator')();
var factory    = require('../util/factory')();

module.exports = function worker(request){

    worker.req = request;

    worker.converter = function(parameters, mandatory){
        worker.req.converted = worker.req.converted || {};

        for (var i in parameters){

            if (mandatory){
                validator.notNull(worker.req.params[parameters[i]], "mandatory url parameter "+ parameters[i] + "not in request", function(validation){
                    if (validation !== true) throw new Error(validation)
                });
            }

            worker.req.converted[parameters[i]] = factory.get(parameters[i])().convert(worker.req.params, parameters[i]);

            validator.valid( worker.req.converted[parameters[i]], function(validation){
                if (! validation === true) throw new Error(validation)
            });
        }
    };

    worker.convertOptional = function(definition){
        if (definition.params.optional) worker.converter(definition.params.optional.split(','))

    };

    worker.convertMandatory = function(definition){
       if (definition.params.mandatory) worker.converter(definition.params.mandatory.split(','), true)
    };

    worker.convert = function(definition){

        worker.convertMandatory(definition);
        worker.convertOptional(definition);

        return worker.req;
    };

    return worker;
};