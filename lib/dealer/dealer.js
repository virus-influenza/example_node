/**
 * @author virus.influenza
 */
var events  = require('events');

module.exports = function(){
    var dealer = new events.EventEmitter();

    dealer.on('request_success', function(response, document){
        console.log("request_success");

        dealer.emit('response', response, 200, document);
    });

    dealer.on('request_failure', function(response, error){
        var status;
        if(error.httpStatus){
            status = error.httpStatus
        }else{
            status = 500
        }

        dealer.emit('response', response, status, error);
    });

    dealer.on('request_empty', function(response){
        dealer.emit('response', response, 204)
    });


    dealer.on('response',function(response, status, document){

        if( document && document.json ){

            response.setHeader('Content-Type',"application/hal+json");
            response.status(status).json(document.json());

            return
        }


        response.status(status).end(JSON.stringify(document))
    });

    return dealer
}();
