/**
 * @author virus.influenza
 */
module.exports = function (req, res, next) {
    console.log("content negotiation filter");

    if (!(req.header("Accept")) || !(req.header("Accept") === 'application/hal+json')) {
        res.set({
            "Content-Type": "application/json",
            "charset": "UTF-8"
        });

        res.status(406).json({
            "message": "Supported Media Type: Accept: application/hal+json, found " + req.header("Accept")
        });

        return
    }

    next();

};