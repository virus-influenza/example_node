module.exports = function (err, req, res, next) {

    console.log("error handler filter");

    if (res.headersSent) {
        return next(err);
    }

    res.set({
        "Content-type":"application/json"
    });

    console.log(err);

    var response = [];
    for (var i in err){
        response[i] = {path:err[i].path, message:err[i].message}
    }

    res.status(400).json( { errors: response });
};