/**
 * @author virus.influenza
 */
module.exports = function (req, res, next) {

    console.log("nsa filter");

    if (!(req.header("x-kokoriko")) || !(req.header("x-kokoriko") === 'putogriego')) {

        res.set({
            "Content-type":"application/json"
        });

        res.status(403).json({
            "message": "this aren't the droids you're looking for",
            "details": "Star Wars: Episode IV - A New Hope. Ben Obi-Wan Kenobi"
        });

        return
    }

    next();
};