var router = require('express').Router();

var validator  = require('../../util/validator')();
var formatter  = require('../../util/formatter')();
var factory    = require('../../util/factory')();

var converter  = require('../../converter/httpmessageconverter');

var definition = {
    name   : "geo",
    context: "search",
    path   : "foodpoints",
    params : {
        mandatory:"geo"
    },
    notNull: "geo",
    query  : "page,size,sort"
};
module.exports = function (context) {

    validator.notNull(context.services.search,"can not initialize router geo with empty autowired services");

    // #Autowired
    var search = context.services.search;

    // router get
    router.get( formatter.
                    express.
                        asURLTemplate(definition.path, definition.params.mandatory, definition.params.optional) , function (req, res) {

            // request validation
            validator.notNull(req.params, "empty request params");

            search(definition).byLocationNear(converter(req).convert(definition), res);
        }
    );

    return router
};

module.exports.definition = definition;