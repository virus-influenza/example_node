var schema = require('validate');
var moment = require('moment');

module.exports = function geo(){

    var json  = {};

    function worker(day){
        json.day    = day;

        return worker;
    }

    worker.convert = function(params, key){
        var day = params[key];

        if (!day) {
            day = moment().format('dddd').toUpperCase()
        }

        return worker(day);
    };

    worker.valid = function(){
        return schema({
                day: {
                    type: "string",
                    required: true,
                    match: /(MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY)/
                }
            }
        ).validate(json)
    };

    worker.json = function(){
        return json
    };

    return worker;
};