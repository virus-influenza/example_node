var schema = require('validate');

module.exports = function geo(){

    var json  = {};

    function worker(point,distance){
        json.point    = point;
        json.distance = distance;

        return worker;
    }

    worker.convert = function(params, key){
        var parts = params[key].split(",");

        if (!parts.length === 3) throw new Error("wrong input %s for geo parameter", params[key]);

        return worker(parts[0] + ',' + parts[1], parts[2]);
    };

    worker.valid = function(){
        var validation = schema({
            point:{
                type    : "string",
                required: true,
                match   : /([0-9]+)(\.[0-9]+)(,)([0-9]+)(\.[0-9]+)/,
                message : "point must match x,y with x=real, y=real and not " + worker.json.point
            },
            distance:{
                type    : "string",
                required: true,
                match   :/([0-9]+)(\.[0-9]+)?(km|mi){1}]/,
                message : "distance must match d,[km|mi] with d=real and not "+ worker.json.distance
            }

        }).validate(json);

        return validation.length === 0? true : validation;
    };

    worker.json = function(){
        return json
    };

    return worker;
};