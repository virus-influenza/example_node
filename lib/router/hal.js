/**
 * @author virus.influenza
 */
var router = require('express').Router();
var Path   = require('path');

var definition = {
    name   : "hal",
    context: 'search',
    path   : "foodpoints"
};
module.exports = function(){

    var hal = require('../service/hal');

    // #routes
    // ##root
    router
        .get(Path.join('/'), function(req, res) {
            res.json( hal.Json()(req)._base(definition) );
        }
    );
    // ##context
    router
        .get(Path.join('/', definition.context), function(req, res) {
            res.json( hal.Json()(req, definition)._context(definition) );
        }
    );

    // ## resource routers
    // geo router
    var geoRouterDefinition            = require('../router/api/geo').definition;
    var geoRouterCriteriaDayDefinition = require('../router/api/geoCriteriaDay').definition;
    router
        .get(Path.join('/', definition.context, definition.path), function(req, res) {
            res.json( hal.Json()(req)._resource(
                [geoRouterDefinition,geoRouterCriteriaDayDefinition]
            ));
        }
    );

    // geo criteria router

    return router;
};

module.exports.definition = definition;

