/**
 * @author virus.influenza
 */
var dealer     = require('../dealer/dealer');

module.exports = function(definition){

    var foodpoint = require('./domain/foodpoint')();

    return function callback(req,res){

        return function(error, document) {
            if (error) {
                console.error('error: ' + JSON.stringify(error));
                dealer.emit('request_failure', res, error);

            } else {
                if(document && document.page && document.page.totalElements == 0){
                    console.log("request empty");

                    return dealer.emit('request_empty', res)
                }

                foodpoint.convert(definition, document, req, true, function(foodpoints){
                    return dealer.emit('request_success', res, foodpoints);
                });

            }
        }
    }
};

