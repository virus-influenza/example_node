var schema = require('validate');
var link   = require('./link');


module.exports = function foodpoint(definition) {
    var json  = {};

    function worker(embedded, links, page, source) {
        json._embedded = embedded;
        json._links    = links;
        json.page     = page;
        json.source   = source;

        return worker;
    }

    worker.convert = function (definition, document, req, includeSource, callback) {
        // native properties simple converter
        var embedded = ( document && document._embedded ) ? document._embedded : false;
        var page     = ( document && document.page ) ? document.page : false;
        var source   = ( includeSource && document) ? document : false;

        // complex property converter
        link(definition).convert(document._links, req, function(links){

            callback( worker(embedded, links.json(), page, source) )
        });
    };

    worker.valid = function () {
        var validation = schema({
            links: {
                type: "Object",
                required: true
            }
        }).validate(json);

        return validation.length === 0 ? true : validation;
    };

    worker.json = function(){
        return json;
    };

    return worker;
};

