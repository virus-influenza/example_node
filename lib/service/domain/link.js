var schema       = require('validate');
var url_template = require('url-template');
var requestInfo  = require('../../util/requestInfo')();

module.exports = function links(definition){

    // private attributes
    var _this      = worker;
    var hal        = require('../hal');
    var json       = {};

    // private methods
    function nextPage(current_page) {
        return Number(current_page) + 1;
    }
    function prevPage(current_page) {
        return (current_page > 0)? Number(current_page) - 1:current_page;
    }

    // constructor
    function worker(self, prev, next){
        json.self = self;
        json.prev = prev;
        json.next = next;

        return worker;
    }

    // api
    worker.convert = function(links, req, callback){

        var apiUrlTemplate = hal.Urls()( requestInfo.fromHttpRequest(req) )._api(definition);

        var self = {
            "href": url_template
                .parse(apiUrlTemplate)
                .expand({
                    geo:      req.params.geo,
                    date:     req.params.date || undefined,
                    page:     req.query.page || undefined,
                    size:     req.query.size || undefined,
                    sort:     req.query.sort || undefined,
                    criteria: req.params.criteria || undefined
                }),
            "templated": true
        };

        if (links.prev) {

            var prev = {
                "href": url_template
                    .parse(apiUrlTemplate)
                    .expand({
                        geo:      req.params.geo,
                        date:     req.params.date || undefined,
                        page:     prevPage(req.query.page) !== ( undefined && 0 ) ? prevPage(req.query.page): undefined,
                        size:     req.query.size || undefined,
                        sort:     req.query.sort || undefined,
                        criteria: req.params.criteria || undefined
                    }),
                "templated": true
            }
        }

        if (links.next) {

            var next = {
                "href": url_template
                    .parse(apiUrlTemplate)
                    .expand({
                        geo:      req.params.geo,
                        date:     req.params.date || undefined,
                        page:     nextPage(req.query.page),
                        size:     req.query.size || undefined,
                        sort:     req.query.sort || undefined,
                        criteria: req.params.criteria || undefined
                    }),
                "templated": true
            }
        }

        return callback( worker(self, prev, next) );
    };

    worker.valid = function(){
        var validation = schema({
            links:{
                type    : "Object",
                required: true
            }
        }).validate(_this);

        return validation.length === 0 ? true : validation;
    };

    worker.json = function(){
        return json;
    };


    return worker;
};

