/**
 * @author virus.influenza
 */
var urljoin     = require('url-join');
var formatter   = require('../util/formatter')();
var requestInfo = require('../util/requestInfo')();
var validator  = require('../util/validator')();

// # this hal service should be able to be used for every api definition
var urls = function () {

    // var definition = require('../router/api/geo').definition;

    function urls(reqInfo) {
        console.log("reqinfo full host in urls : "+ reqInfo.fullHost() );

        urls.reqInfo    = reqInfo;

        return urls;
    }

    urls._context = function (definition) {

        return urljoin(urls.reqInfo.fullHost() , definition.context)
    };

    urls._resource = function (definition) {
        return urljoin(urls._context(definition), definition.path)
    };

    // params y query
    urls._api = function (definition) {
        definition.params = definition.params || {};

        return urljoin(
            urls._resource(definition),
            formatter.hal.asURLTemplate(
                definition.params.mandatory || '',
                definition.params.optional  || '',
                definition.query            || ''
            )
        )
    };

    return urls;
};

var json = function () {

    function hal(req) {
        hal.reqInfo = requestInfo.fromHttpRequest( req );

        return hal;
    }

    // displays next link
    hal._base = function (definition) {
        return {
            "_links": {
                "search": {
                    "href": urls()( hal.reqInfo )._context(definition),
                    "templated": false
                }
            }
        }

    };

    hal._context = function (definition) {
        return {
            "_links": {
                "foodpoints": {
                    "href": urls()( hal.reqInfo )._resource(definition),
                    "templated": false
                }
            }
        }
    };

    hal._resource = function (definitions) {

        var result = {"_links":{}};
        for(var i in definitions){
            result._links[definitions[i].name] = {
                "href": urls()( hal.reqInfo )._api(definitions[i]),
                "templated": true
            }
        }

        return result
    };

    return hal;
};

module.exports.Urls = urls;
module.exports.Json = json;
