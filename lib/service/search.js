/**
 * @author virus.influenza
 */
var extend         = require('extend');
var traverson      = require('traverson');
var JsonHalAdapter = require('traverson-hal');

// register the traverson-hal plug-in for media type 'application/hal+json'
traverson.registerMediaType(JsonHalAdapter.mediaType, JsonHalAdapter);

var validator      = require('../util/validator')();

// public service api
module.exports = function(context){

    validator.notNull(context, "can not initialize search service with no configuration");

    validator.notNull(context.config.get("definition.api.search"), "can not initialize search service with api definition");

    // configuration
    var api = context.config.get("definition.api");
    validator.notNull(api.search.target,"can not initialize search service with no api endpoint target");

    // injected
    var discovery = context.eureka.discovery;
    var targetUrl = discovery.getInstancesByAppId(api.search.target)[0].homePageUrl;
    console.log("target url : " + targetUrl);
    validator.notNull(targetUrl,"endopoint is down " + targetUrl);

    // dependencies
    var callback  = require('./callback');

    /**
     * API
     */
    function search(endPointDefinition){

        // self endpoint definition
        validator.notNull(endPointDefinition,"self endPointDefinition is undefined, can not create proper hal response");
        search.endPointDefinition = endPointDefinition;

        return search;
    }

    /**
     * req.converted.geo (point,distance,query)
     * @param req
     * @param res
     */
    search.byLocationNear = function(req,res) {

        validator.notNull(req.converted, "req.converted is mandatory");
        validator.notNull(req.converted.geo, "req.converted.geo is mandatory for byLocationNear search");

        traverson
            .from(targetUrl)
            .jsonHal()
            .follow(['foodpoints','search','byLocationNear'])
            .withTemplateParameters(extend( req.converted.geo.json(), req.query ))
            .getResource(
                callback(search.endPointDefinition)(req,res)
            );
    };


    search.bySphereAndCriteriaAndDay = function(req,res){
        validator.notNull(req.converted, "req.converted is mandatory");
        validator.notNull(req.converted.sphere, "req.converted.sphere is mandatory for bySphereAndCriteriaAndDay search");
        validator.notNull(req.converted.criteria, "req.converted.criteria mandatory for bySphereAndCriteriaAndDay search");
        validator.notNull(req.converted.date, "req.converted.date is mandatory for bySphereAndCriteriaAndDay search");

        traverson
            .from(targetUrl)
            .jsonHal()
            .follow(['foodpoints','search','byDayAndLocationWithinAndCriteria'])
            .withTemplateParameters(extend(
                req.converted.sphere.json(),
                req.converted.criteria.json(),
                req.converted.date.json(),
                req.query
            ))
            .getResource(
                callback(search.endPointDefinition)(req,res)
            );
    };

    return search;
};