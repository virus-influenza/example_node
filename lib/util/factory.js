function noop(){

    var json = {};

    var worker = function(param,key){
        json[key] = param;

        return worker
    };

    worker.convert = function(params,key){
        return worker(params[key], key);
    };

    worker.valid = function(){
        return true;
    };

    worker.json = function(){
        return json;
    };

    return worker;
}

module.exports = function factory() {

    var worker = function(){};

    worker.config = {
        geo: function () {
            return require('./../router/api/params/geo');
        },
        date: function(){
            return require('./../router/api/params/date');
        }
    };


    worker.get = function(param){
        var result = worker.config[param];

        if (result === undefined){
            return noop(); // default converter does nothing
        }

        return result();
    };

    return worker;
};