var urljoin = require('url-join');

module.exports = function(){

    function worker(){}

    worker.express = function(){};
    worker.hal     = function(){};

    /**
     * express
     */
    worker.express.asURLTemplate = function(path, mandatory, optional){

        return urljoin('/', path , worker.express.asURLParams(mandatory, optional) );
    };

    worker.express.asURLParams = function(mandatory, optional){

        return urljoin('/',
            mandatory.split(',').map(function(obj){
                return "/:"+obj}).join(''),

            optional? optional.split(',').map(function(obj){
                return "/:"+obj+"?"
            }).join('') : '')
    };


    /**
     * hal
     */
    worker.hal.asURLTemplate = function(mandatory, optional, query){
        return urljoin(worker.hal.asTemplateParams(mandatory, optional), worker.hal.asTemplateQueryString(query))
    };

    worker.hal.asTemplateParams = function(mandatory, optional){

        return urljoin('/',
            mandatory.split(',').map(function(obj){
                return obj.replace(/(.*)/, "\/\{$1\}")
            }).join(''),

            optional? optional.split(',').map(function(obj){
                return obj.replace(/(.*)/, "\{\/$1\}")
            }).join('') : ''
        )
    };

    worker.hal.asTemplateQueryString = function(query){

        return urljoin(query.replace(/(.*)/, "\{\?$1\}") );
    };


    return worker
};