var urljoin = require("url-join");

module.exports = function requestInfo(){

    var worker = function(protocol, host, port){
        worker.protocol = protocol;
        worker.host     = host;

        return worker;
    };

    worker.fromHttpRequest = function(request){

        console.log("worker.fromHttpRequest");
        console.log("worker.fromHttpRequest request protocol: " + request.protocol);
        console.log("worker.fromHttpRequest request host    : " + request.get('host'));

        if (!request || !request.protocol || ! request.get("host")) return worker();

        return worker(request.protocol+':',request.get("host")); // host + port
    };

    worker.fullHost = function(){
        if (!worker.protocol || !worker.host ) return "/";

        return urljoin(worker.protocol, worker.host)
    };

    return worker;
};