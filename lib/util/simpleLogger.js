/**
 * Created by virus on 11/6/15.
 */
module.exports;

const chalk = require('chalk');

module.exports.error = chalk.bgBlack.bold.red;
module.exports.debug = chalk.gray;
module.exports.title = chalk.underline.gray;