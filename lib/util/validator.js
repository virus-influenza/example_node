function def(validate){
    throw new Error(validate);
}
module.exports = function(){


    function worker(){}

    worker.notNull = function(input, message, callback){
        var cause = message || "validation error input is null";
        var cb    = callback|| def;

        if (!input || input === undefined) return cb(cause);
    };

    worker.valid = function(object, callback){
        var cb    = callback|| def;

        if (!object) return true; // is the non existence valid?

        var validate = object.valid();

        if (!validate === true) callback(validate)
    };

    return worker
};