/**
 * @author virus.influenza
 */
var sinon = require('sinon');
var chai  = require('chai');

var expect     = chai.expect;
var rewire     = require("rewire");
var callback   = rewire('../../../lib/service/callback');
var dealer     = require('../../../lib/dealer/dealer');
var foodpoints = require('../../../lib/service/domain/foodpoint');

var document = require('./mocks/document');


describe('./lib/service/callback', function () {

    var mockDealer = {};

    before(function () {
    });

    beforeEach(function () {
    });

    describe('on response error', function(){

        it('should emit request_failure event with response and error data', function(){

            var error    =   { message:"hello human i am an error" };
            var request  = sinon.stub();
            var response = sinon.stub();

            // mocks
            mockDealer.emit = sinon.stub(dealer, "emit").withArgs("request_failure", response, error);

            // rewire
            callback.__set__("dealer", mockDealer);

            // actual call, with error
            callback(request, response)(error, {});

            // expectations
            expect(mockDealer.emit.calledOnce).to.equal(true);
            expect(mockDealer.emit.getCall(0).args[0]).to.equal("request_failure");
            expect(mockDealer.emit.getCall(0).args[1]).to.equal(response);
            expect(mockDealer.emit.getCall(0).args[2]).to.equal(error);

            dealer.emit.restore()
        })

    });

    describe('on response empty', function(){

        it('should emit request_empty event with response data', function(){

            var request  = sinon.stub();
            var response = sinon.stub();

            // mocks
            var empty = {page:{totalElements:0}};
            mockDealer.emit    = sinon.stub(dealer, "emit").withArgs("request_empty", response);

            // rewire mocks
            callback.__set__("dealer", mockDealer);

            // actual call, undefined error but empty document
            callback(request, response)(undefined, empty);

            // expectations
            expect(mockDealer.emit.calledOnce).to.equal(true);
            expect(mockDealer.emit.getCall(0).args[0]).to.equal("request_empty");
            expect(mockDealer.emit.getCall(0).args[1]).to.equal(response);

            // restore
            dealer.emit.restore()
        })

    });


    describe('on document', function(){

        it('should emit request_success event with response and document data', function(){

            var request  = sinon.stub();
            var response = sinon.stub();

            // mocks
            var builder = sinon.stub().returns(document);
            var mockfoodpoints = sinon.stub().withArgs(document, request).returns({build:builder});
            sinon.stub(dealer, "emit").withArgs("request_success", response, document);

            // rewire dependencies
            callback.__set__("dealer", dealer);
            callback.__set__("foodpoints", mockfoodpoints);

            // actual call, undefined error and document
            callback(request, response)(undefined, document);

            // expectations
            expect(dealer.emit.calledOnce).to.equal(true);
            expect(dealer.emit.getCall(0).args[0]).to.equal("request_success");
            expect(dealer.emit.getCall(0).args[1]).to.equal(response);
            expect(dealer.emit.getCall(0).args[2]).to.equal(document);

            expect(mockfoodpoints.calledOnce).to.equal(true);
            expect(mockfoodpoints.getCall(0).args[0]).to.equal(document);
            expect(mockfoodpoints.getCall(0).args[1]).to.equal(request);

            expect(builder.calledOnce).to.equal(true)

            // restore
            dealer.emit.restore();
        })

    });

    afterEach(function(){
    });

    after(function () {
    });
});
