/**
 * @author virus.influenza
 */
var chai = require('chai');
var querystring = require('querystring');

var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();

var sut = require('../../../lib/service/domain/foodpoint');
var request = require('./mocks/request');
var document = require('./mocks/document');

describe('./lib/service/foodpoints', function () {
    var foodpoint;

    before(function () {
    });

    beforeEach(function () {
        foodpoint = sut(document(), request());
    });

    describe('mandanga', function(){
        it('smokes mandanga when goes to the fiesta', function () {
            var mandanga = true;

            expect(mandanga).to.equal(true);
            mandanga.should.equal(true);
            assert.equal(mandanga, true);
        });
    });

    describe('build', function () {

        it('should correctly build a foodpoint search json hal', function () {
            expect(foodpoint).to.not.equal(undefined);

            expect(foodpoint.build()).to.have.property("_embedded");
            expect(foodpoint.build()).to.have.property("_links");
            expect(foodpoint.build()).to.have.property("page");

        })

    });

    describe('_embedded', function(){

        it('should correctly build a foodpoint search json hal _embedded', function(){
            expect(foodpoint.build()).to.have.property("_embedded")
                .that.is.an('object')
                .that.deep.equals({ "foodpoints": [
                    {
                        "id": 2.647324051386731e+28,
                        "name": "test unit food point one",
                        "owner": "unit@speiseplan.testers",
                        "zipCode": "10245",
                        "score": 0,
                        "day": "TUESDAY",
                        "searchable": {
                            "root": "VEGAN",
                            "main": "FASTFOOD",
                            "sub": "PIZZA ITALIAN MAMAMIA",
                            "menu": "vegan pizza caneloni proscuito"
                        },
                        "position": [
                            1,
                            1
                        ],
                        "_links": {
                            "self": {
                                "href": "http://mgimmy.speiseplan:10201/foodpoints/26473240513867312455279521070"
                            }
                        }
                    }
                ] });
        })

    });

    describe('_links', function(){

        it('should correctly build a foodpoint search json hal _links', function(){
            expect(foodpoint.build()).to.have.property("_links")
                .that.is.an('object')
                .that.deep.equals({
                    "self": {
                        "href": "http://localhost:8080/search/foodpoints/" + querystring.escape("1,1,1km") + "?page=1&size=1",
                        "templated": true
                    },
                    "next": {
                        "href": "http://localhost:8080/search/foodpoints/" + querystring.escape("1,1,1km") + "?page=2&size=1",
                        "templated": true
                    },
                    "prev": {
                        "href": "http://localhost:8080/search/foodpoints/" + querystring.escape("1,1,1km") + "?page=0&size=1",
                        "templated": true
                    }
                });
        })

    });

    describe('page', function(){

        it('should correctly build a foodpoint search json hal page', function(){
            expect(foodpoint.build()).to.have.property("page")
                .that.is.an('object')
                .that.deep.equals({
                    "size": 1,
                    "totalElements": 10,
                    "totalPages": 10,
                    "number": 1
                });

        })

    });

    after(function () {
        // after() is run after all your tests have completed. Do teardown here.
    });
});