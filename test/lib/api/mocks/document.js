/**
 * @author virus.influenza
 */
module.exports = function(){

    return {

        page: {
            "size": 1,
            "totalElements": 10,
            "totalPages": 10,
            "number": 1
        },

        _embedded: {
            "foodpoints": [
                {
                    "id": 2.647324051386731e+28,
                    "name": "test unit food point one",
                    "owner": "unit@speiseplan.testers",
                    "zipCode": "10245",
                    "score": 0,
                    "day": "TUESDAY",
                    "searchable": {
                        "root": "VEGAN",
                        "main": "FASTFOOD",
                        "sub": "PIZZA ITALIAN MAMAMIA",
                        "menu": "vegan pizza caneloni proscuito"
                    },
                    "position": [
                        1,
                        1
                    ],
                    "_links": {
                        "self": {
                            "href": "http://mgimmy.speiseplan:10201/foodpoints/26473240513867312455279521070"
                        }
                    }
                }
            ]
        },

        _links: {
            "self": {
                "href": "http://mgimmy.speiseplan:10201/foodpoints/search/byPositionNear?point=1%2C1&distance=1km&page=1&size=1{&sort}",
                "templated": true
            },
            "next": {
                "href": "http://mgimmy.speiseplan:10201/foodpoints/search/byPositionNear?point=1%2C1&distance=1km&page=2&size=1{&sort}",
                "templated": true
            },
            "prev": {
                "href": "http://mgimmy.speiseplan:10201/foodpoints/search/byPositionNear?point=1%2C1&distance=1km&page=0&size=1{&sort}",
                "templated": true
            }
        }
    };

};