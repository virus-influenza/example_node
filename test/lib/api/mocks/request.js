/**
 * @author virus.influenza
 */
module.exports = function(){

    return {
        params: {
            geo     : "1,1,1km",
            criteria: "",
            date    : ""
        },
        query: {
            page: 1,
            size: 1,
            sort: ''
        }

    };

};