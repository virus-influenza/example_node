/**
 * @author virus.influenza
 */
var chai = require('chai');

var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();

var sut         = require('../../../lib/service/hal');
var requestInfo = require('../../../lib/util/requestInfo')();

describe('./lib/service/definition', function () {

    var mockedConf;
    var urls;
    var hal;

    before(function () {
        mockedConf = {
            "get": function(key){
                if (key==="definition.api.search") return {

                    resource:"myResource",
                    params  :"param1,(param2)",
                    query   :"page,size,sort"
                };

                if (key==="server") return {

                    contexPath:"myContext"
                };
            }
        };

        hal  = sut.Hal(mockedConf);
    });

    beforeEach(function () {
    });

    describe('mandanga', function(){
        it('smokes mandanga when goes to the fiesta', function () {
            var mandanga = true;

            expect(mandanga).to.equal(true);
            mandanga.should.equal(true);
            assert.equal(mandanga, true);
        });
    });

    describe('definition.Urls', function(){
        it('should return HAL templated urls with request info', function () {
            urls = sut.Urls(mockedConf, requestInfo("http:","myDomain",8080));


            expect(urls._context()).to.equal("http://myDomain:8080/myContext");
            expect(urls._resource()).to.equal("http://myDomain:8080/myContext/myResource");
            expect(urls._api()).to.equal("http://myDomain:8080/myContext/myResource/{param1}{/param2}/{?page,size,sort}");
        });

        it('should return HAL templated urls', function () {
            urls = sut.Urls(mockedConf, requestInfo());


            expect(urls._context()).to.equal("/myContext");
            expect(urls._resource()).to.equal("/myContext/myResource");
            expect(urls._api()).to.equal("/myContext/myResource/{param1}{/param2}/{?page,size,sort}");
        });
    });


    after(function () {
        // after() is run after all your tests have completed. Do teardown here.
    });
});