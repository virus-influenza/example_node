/**
 * @author virus.influenza
 */
var chai = require('chai');
var querystring = require('querystring');

var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();

var sut    = require('../../../lib/util/formatter');

describe('./lib/service/foodpoints', function () {

    var formatter = sut();

    before(function () {
    });

    beforeEach(function () {
    });

    describe('mandanga', function(){
        it('smokes mandanga when goes to the fiesta', function () {
            var mandanga = true;

            expect(mandanga).to.equal(true);
            mandanga.should.equal(true);
            assert.equal(mandanga, true);
        });
    });

    describe('worker.hal.asURLTemplate', function(){

        it('should return a parsed hal url template', function(){

            var result = formatter.hal.asTemplateParams("param1,(param2),param3");

            expect(result).to.equal("/{param1}{/param2}/{param3}")

        })

    });

    describe('worker.hal.asTemplateQueryString', function(){

        it('should return a parsed hal url template', function(){

            var result = formatter.hal.asTemplateQueryString("query1,query2");

            expect(result).to.equal("{?query1,query2}")

        })

    });

    after(function () {
        // after() is run after all your tests have completed. Do teardown here.
    });
});