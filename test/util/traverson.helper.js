var api            = require('traverson');
var JsonHalAdapter = require('traverson-hal');
var from           = "http://speiseplan.menu:5001/service/follow";

api.registerMediaType(JsonHalAdapter.mediaType, JsonHalAdapter);
api
    .from(from)
    .jsonHal()
    .withRequestOptions({
        headers: {
            'Accept': 'application/hal+json',
            'x-kokoriko': 'putogriego'
        }
    })
    .follow(['search','foodpoints','api'])
    .withTemplateParameters( {geo:"13.404954,52.5200066,10km",criteria:"vegan,ceviche,kokoriko",date:"MONDAY"} )
    .getResource(function(error, document){

        console.log(error);
        console.log(document);

    });